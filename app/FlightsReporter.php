<?php
declare(strict_types=1);

namespace App;

use Carbon\Carbon;
use Tightenco\Collect\Support\Collection;

class FlightsReporter
{
    // seconds
    protected $maxIntervalBetweenRouteFlights = 60 * 60 * 24 * 10;

    protected $flights, $routes;

    /**
     * FlightsReporter constructor.
     * @param array $flights
     */
    public function __construct(array $flights)
    {
        $this->flights = self::sortFlightsByDepart(collect($flights));
        $this->routes = $this->parseRoutes();
    }

    /**
     * @return mixed
     */
    public function getLongestRouteInfo() {
        $route = $this->routes->sortByDesc(function (Route $route) {
            return $route->getTimeMinutes();
        })->first();
        return $route->getInfo();
    }

    /**
     * @return Collection
     * */
    protected function parseRoutes(): Collection {
        $routes = [];
        $flights = $this->flights;
        foreach ($flights as $k => $flight) {
            $routes[] = $this->getRoute($flight, $flights);
        }
        return collect($routes);
    }

    /**
     * @param array $flight
     * @param Collection $flights
     * @return Route
     */
    protected function getRoute(array $flight, Collection $flights): Route
    {
        $routeFlights = collect([ $flight ]);
        do {
            $nextPoint = $this->getNextPoint($routeFlights, $flights);
            if($nextPoint) $routeFlights->push($nextPoint);
        } while($nextPoint);
        $route = new Route();
        $route->setFlights($routeFlights);
        return $route;
    }

    /**
     * @param Collection $route
     * @param Collection $flights
     * @return array|null
     */
    protected function getNextPoint(Collection $route, Collection $flights): ?array
    {
        $lastRoutePoint = self::sortFlightsByDepart($route)->last();
        $nextPoint = $flights->first(function ($flight) use ($lastRoutePoint) {
           return  $lastRoutePoint['to'] == $flight['from'] && $this->isRoutePoints($lastRoutePoint, $flight);
        });
        return $nextPoint ?? null;
    }

    /**
     * @param $pointA
     * @param $pointB
     * @return bool
     */
    protected function isRoutePoints($pointA, $pointB) : bool
    {
        $pointA = Carbon::create($pointA['arrival']);
        $pointB = Carbon::create($pointB['depart']);
        if($pointA > $pointB) return false;
        $diffSeconds = $pointA->diffInSeconds($pointB);
        return $diffSeconds > 0 && $diffSeconds <= $this->maxIntervalBetweenRouteFlights;
    }

    /**
     * @param Collection $flights
     * @return Collection
     */
    static function sortFlightsByDepart(Collection $flights): Collection
    {
        return $flights->sortBy(function ($flight) {
            return Carbon::create($flight['depart'])->getTimestamp();
        });
    }

}