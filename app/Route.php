<?php

namespace App;

use \Tightenco\Collect\Support\Collection;
use Carbon\Carbon;

class Route
{
    protected $flights;
    public $timeMinutes;
    public $info;

    public function setFlights(Collection $flights)
    {
        $this->flights = collect($flights);
        $this->timeMinutes = $this->getTimeMinutes();
    }

    public function getFlights() : Collection
    {
        return \App\FlightsReporter::sortFlightsByDepart($this->flights);
    }

    /**
     * @return int
     */
    public function getTimeMinutes() : int
    {
        if(!$this->timeMinutes) {
            $flights = $this->getFlights();
            $this->timeMinutes = Carbon::create($flights->first()['depart'])->diffInMinutes(Carbon::create($flights->last()['arrival']));
        }
        return $this->timeMinutes;
    }

    /**
     * @return string
     */
    public function getInfo() : string
    {
        $flights = $this->getFlights();
        if(!$this->info) $this->info = $flights->first()['from'] . ' ' . $flights->last()['to'] . ' ( '. $this->getTimeMinutes() . ' минут)';
        return $this->info;
    }


}