<?php

require 'vendor/autoload.php';

use App\FlightsReporter;

$flights = require 'flights.php';

$reporter = new FlightsReporter($flights);

echo $reporter->getLongestRouteInfo();
